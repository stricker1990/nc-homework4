package ru.pavlovka.nchomework4.transport;

import java.util.Objects;

public class Car {
    private double gallonsPerMile;
    private double x;
    private double currentFuel;

    public Car(double gallonsPerMile){
        this(gallonsPerMile, 0);
    }

    public Car(double gallonsPerMile, double fuel){
        this(0, gallonsPerMile, fuel);
    }

    public Car(double x, double gallonsPerMile, double fuel){
        this.gallonsPerMile = gallonsPerMile;
        this.x = x;
        this.currentFuel = fuel;
    }

    public double getGallonsPerMile() {
        return gallonsPerMile;
    }

    public void setGallonsPerMile(double gallonsPerMile) {
        this.gallonsPerMile = gallonsPerMile;
    }

    public double getCurrentFuel() {
        return currentFuel;
    }

    public double getX() {
        return x;
    }

    public void refuel(double fuel){
        currentFuel += fuel;
    }

    public void drive(double miles){
        double maxMiles = Math.min(canDriveMiles(), miles);
        x += maxMiles;
        currentFuel -= fuelConsumption(maxMiles);
    }

    public double canDriveMiles(){
        return canDriveMiles(this.currentFuel);
    }

    public double canDriveMiles(double fuel){
        return fuel/gallonsPerMile;
    }

    public double fuelConsumption(double miles){
        return miles*gallonsPerMile;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return Double.compare(car.gallonsPerMile, gallonsPerMile) == 0 &&
                Double.compare(car.x, x) == 0 &&
                Double.compare(car.currentFuel, currentFuel) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(gallonsPerMile, x, currentFuel);
    }

    @Override
    public String toString() {
        return "Car{" +
                "gallonsPerMile=" + gallonsPerMile +
                ", x=" + x +
                ", currentFuel=" + currentFuel +
                '}';
    }
}
