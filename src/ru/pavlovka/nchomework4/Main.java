package ru.pavlovka.nchomework4;

import ru.pavlovka.nchomework4.geometry.Point;
import ru.pavlovka.nchomework4.transport.Car;

public class Main {

    public static void main(String[] args) {
        testPoints();
        testCar();
    }

    public static void testPoints(){
        System.out.println(new Point(3, 4).translate(1,3).scale(0.5));
    }

    public static void testCar(){
        Car car = new Car(0.5, 10);
        car.drive(25);
        System.out.println(car);
    }
}
